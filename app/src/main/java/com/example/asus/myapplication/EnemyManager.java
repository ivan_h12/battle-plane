package com.example.asus.myapplication;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import java.util.ArrayList;

public class EnemyManager {
    private ArrayList<Enemy> enemies;
    private int enemyWidth;
    private int enemyHeight;
    private int color;
    private int enemyGap;

    private long startTime;
    private long initTime;
    private long enemyShowTime;

    private Bitmap spritesheet;

    public static int score = 0;

    public EnemyManager(int enemyWidth, int enemyHeight, int color, int enemyGap, Bitmap res){
        this.enemyWidth = enemyWidth;
        this.enemyHeight = enemyHeight;
        this.color = color;
        this.enemyGap = enemyGap;
        spritesheet = res;
        spritesheet = Bitmap.createScaledBitmap(spritesheet, enemyWidth + 50, enemyHeight + 50, false);

        startTime = initTime = System.currentTimeMillis();

        enemies = new ArrayList<>();

        populateObstacles();
    }

    public boolean playerCollide(Player player){
        for(Enemy enemy : enemies){
            if(enemy.playerCollide(player)) {
                enemies.remove(enemy);
                return true;
            }
        }
        return false;
    }
    public boolean bulletCollide(Bullet bullet){
        for(Enemy enemy : enemies){
            if(enemy.bulletCollide(bullet)){
                enemies.remove(enemy);
                return true;
            }
        }
        return false;
    }

    private void populateObstacles(){
        /*enemyShowTime += (int)((System.currentTimeMillis() - initTime) / 1000);
        if(enemyShowTime >= 1){
            enemies.add(new Enemy(enemyWidth, enemyHeight, color, 0));
            enemyShowTime -= 1;
        }*/
        int currY = -10*Constant.SCREEN_HEIGHT;
        while(currY < 0){
            enemies.add(new Enemy(enemyWidth, enemyHeight, color, currY));
            currY += (enemyHeight + enemyGap) * 2 ;
        }

    }

    public void update(){
        int elapsedTime = (int)(System.currentTimeMillis() - startTime);

        startTime = System.currentTimeMillis();
        float speed = (float)(Math.sqrt(1 + (startTime - initTime)/4000.0f))*Constant.SCREEN_HEIGHT/(10000.0f);
        for(Enemy enemy : enemies){
            enemy.incrementY(speed * elapsedTime);
        }

        if(enemies.get(enemies.size() - 1).getRectangle().top >= Constant.SCREEN_HEIGHT){
            enemies.add(0, (new Enemy(enemyWidth, enemyHeight, color, enemies.get(0).getRectangle().top - enemyHeight - enemyGap)));
            enemies.remove(enemies.size() - 1);
            //score ++;
            Player.HP--;

        }
        /*if(enemies.get(0).getRectangle().top >= Constant.SCREEN_HEIGHT){
            //enemies.add(0, new Enemy(enemyWidth, enemyHeight, color, enemies.get(0).getRectangle().top - enemyHeight - enemyGap));
            enemies.remove(0);
            score ++;
        }*/
        /*if(enemyShowTime >= 1){
            enemies.add(new Enemy(enemyWidth, enemyHeight, color, -5*Constant.SCREEN_HEIGHT/4));
            enemyShowTime -= 1;
        }*/
    }

    public void draw(Canvas canvas){
        for(Enemy enemy : enemies) {
            enemy.draw(canvas);
            canvas.drawBitmap(spritesheet, enemy.getLeft() - 20, enemy.getTop() - 5, null);
        }
        Paint paint = new Paint();
        paint.setTextSize(100);
        paint.setColor(Color.MAGENTA);
        canvas.drawText("" + score, 50, 100 + paint.descent() - paint.descent(), paint);
    }
    public int getscore(){
        return score;
    }
    public ArrayList<Enemy> getEnemy(){
        return enemies;
    }
}
