package com.example.asus.myapplication;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;

import static com.example.asus.myapplication.MainActivity.xAccel;
import static com.example.asus.myapplication.MainActivity.xMax;
import static com.example.asus.myapplication.MainActivity.yAccel;
import static com.example.asus.myapplication.MainActivity.yMax;

public class Player implements GameObject {

    private Rect rectangle;
    private int color;
    private float xPos,yPos,yVel,xVel = 0.0f;
    public static int HP = 10;
    private Bitmap spritesheet;

    public void setHP(int HP){
        this.HP = HP;
    }
    public int getHP(){
        return HP;
    }

    public Rect getRectangle() {
        return rectangle;
    }

    public Player(Rect rectangle, int color, Bitmap res){
        this.rectangle = rectangle;
        this.color = color;
        spritesheet = res;
        spritesheet = Bitmap.createScaledBitmap(spritesheet, rectangle.width() + 50, rectangle.height() + 50, false);

    }

    @Override
    public void draw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(color);
        canvas.drawRect(rectangle,paint);
        canvas.drawBitmap(spritesheet, rectangle.left - 20, rectangle.top - 5, null);
    }

    @Override
    public void update() {

    }

    public void update(Point point){
        float frameTime = 500f;
        xVel += (xAccel* frameTime);
        yVel += (yAccel * frameTime);

        float xS = (xVel / 3) / frameTime;
        float yS = (yVel / 3) / frameTime;

        xPos -= xS;
        yPos -= yS;

        if(xPos > xMax){
            xPos = xMax;
        } else if (xPos <= rectangle.width()/2){
            xPos = rectangle.width()/2;
        }

        if(yPos > yMax){
            yPos = yMax;
        }else if(yPos <= rectangle.height()/2){
            yPos = rectangle.height()/2;
        }
        rectangle.set((int)(xPos - rectangle.width()/2), (int) (yPos - rectangle.height()/2), (int) (xPos + rectangle.width()/2), (int) (yPos + rectangle.height()/2));
    }

}
