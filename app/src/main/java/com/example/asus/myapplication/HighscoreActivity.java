package com.example.asus.myapplication;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.io.File;
import java.util.ArrayList;

public class HighscoreActivity extends Activity {
    private ListView lv_highscore;
    private SQLiteDatabase db;
    DBManager dbmanager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_highscore);
        lv_highscore = (ListView) findViewById(R.id.lv_hs);
        dbmanager = new DBManager(this);
        db = dbmanager.getReadableDatabase();
        dbmanager.onCreate(db);

        if(!dbmanager.isEmpty()) {
            populate();
        }
    }

    private void populate(){
        Cursor data = dbmanager.getHighscores();
        ArrayList<String> listhighscore = new ArrayList<>();
        int counter = 1;
        while(data.moveToNext()){
            String finalText = Integer.toString(counter)+" "+data.getString(0)+" "+data.getString(1);
            listhighscore.add(finalText);
            counter++;
        }
        ListAdapter adapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,listhighscore);
        lv_highscore.setAdapter(adapter);
    }
}
