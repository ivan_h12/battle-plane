package com.example.asus.myapplication;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

import com.example.asus.myapplication.GameObject;

import java.util.Random;

public class Bullet implements GameObject {
    private int x, y, width, height;
    private int score;
    private int speed;
    private Random rand = new Random();
    private Bitmap spritesheet;

    public Bullet(Bitmap res, int x, int y, int w, int h, int s)
    {
        this.x = x;
        this.y = y;
        width = w;
        height = h;


        speed = s;

        //cap bullet speed
        //if(speed>40)speed = 40;

        spritesheet = res;
        spritesheet = Bitmap.createScaledBitmap(spritesheet, width + 50, height + 50, false);


    }
    public void update()
    {
        y-=speed;

    }
    public void draw(Canvas canvas)
    {
        try{
            canvas.drawBitmap(spritesheet,x,y,null);
        }catch(Exception e){}
    }
    public Rect getRectangle(){
        return new Rect(x, y, x+width, y+height);
    }
    public boolean collide(Player player){
        if(Rect.intersects(getRectangle(), player.getRectangle()))
            return true;
        else return false;
    }
    public int getX(){
        return x;
    }






}