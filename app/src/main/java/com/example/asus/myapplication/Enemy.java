package com.example.asus.myapplication;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class Enemy implements GameObject{
    private Rect body;
    private int color;
    private long bulletStartTime;
    private long bulletElapsed;


    public Rect getRectangle(){
        return body;
    }
    public int getTop() {return body.top;}
    public int getLeft() {return body.left;}

    public void incrementY(float Y){
        body.top += Y;
        body.bottom += Y;
    }

    public Enemy(int rectWidth, int rectHeight, int color, int startY){
        this.color = color;
        int X = (int)(Math.random()*(Constant.SCREEN_WIDTH) - rectWidth);
        while(X < rectWidth) { X = (int)(Math.random()*(Constant.SCREEN_WIDTH) - rectWidth); }
        body = new Rect(X, startY, X + rectWidth, startY + rectHeight);
        bulletStartTime = System.nanoTime();
        bulletElapsed = 0;


    }

    public boolean playerCollide(Player player){
        return Rect.intersects(body, player.getRectangle());
    }
    public boolean bulletCollide(Bullet bullet){
        return Rect.intersects(body, bullet.getRectangle());
    }

    @Override
    public void draw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(color);
        canvas.drawRect(body, paint);
    }

    @Override
    public void update() {

    }

    public long getBulletStartTime() {
        return bulletStartTime;
    }

    public void setBulletStartTime(long bulletStartTime) {
        this.bulletStartTime = bulletStartTime;
    }

    public long getBulletElapsed() {
        return bulletElapsed;
    }

    public void setBulletElapsed(long bulletElapsed) {
        this.bulletElapsed = bulletElapsed;
    }
}
