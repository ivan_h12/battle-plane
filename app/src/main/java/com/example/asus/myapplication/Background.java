package com.example.asus.myapplication;

import android.graphics.Bitmap;
import android.graphics.Canvas;

public class Background {
    private Bitmap image;
    private int x, y, dx, dy;

    public Background(Bitmap res)
    {
        image = res;
    }
    public void update()
    {
        /*x+=dx;
        if(x<-Constant.SCREEN_WIDTH){
            x=0;
        }*/

        y+=dy;
        if(y<-Constant.SCREEN_HEIGHT){
            y=0;
        }
    }
    public void draw(Canvas canvas)
    {
        canvas.drawBitmap(image, x, y,null);
        /*if(x<0)
        {
            canvas.drawBitmap(image, x+Constant.SCREEN_WIDTH, y, null);
        }*/
        /*if(y<0)
        {
            canvas.drawBitmap(image, x, y+Constant.SCREEN_HEIGHT, null);
        }*/
    }
    public void setVector(int dy)
    {
        this.dy = dy;
    }
}
