package com.example.asus.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class GameOver extends Activity {

    private EditText name;
    private Button submit;
    private SQLiteDatabase db;
    private EnemyManager enemyManager;
    DBManager dbmanager;
    MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_over);

        mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.gameover);
        mediaPlayer.start();

        name = (EditText) findViewById(R.id.name);
        submit = (Button) findViewById(R.id.submit);

        dbmanager = new DBManager(this);
        db = dbmanager.getWritableDatabase();
        dbmanager.onCreate(db);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int score = getIntent().getIntExtra("SCORE", 0);
                dbmanager.insertdata(db, name.getText().toString(), score);
                Intent gotoMenu = new Intent(GameOver.this, Menu.class);
                startActivity(gotoMenu);
            }
        });
    }
}
