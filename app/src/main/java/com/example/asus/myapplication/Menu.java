package com.example.asus.myapplication;

        import android.app.Activity;
        import android.content.Intent;
        import android.media.MediaPlayer;
        import android.os.Bundle;
        import android.view.View;
        import android.widget.TextView;

public class Menu extends Activity {

    MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.puzzle);
        mediaPlayer.start();

        TextView PlayButton = (TextView) findViewById(R.id.play);
        PlayButton.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View view) {
                                              Intent gotoPlay = new Intent(Menu.this, MainActivity.class);
                                              startActivity(gotoPlay);
                                          }
                                      }
        );

        TextView HighscoreButton = (TextView) findViewById(R.id.highscore);
        HighscoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent gotoHs = new Intent(Menu.this, HighscoreActivity.class);
                startActivity(gotoHs);
            }
        });

        TextView ExitButton = (TextView) findViewById(R.id.exit);
        ExitButton.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View view) {
                                              finishAffinity();
                                              System.exit(0);
                                          }
                                      }
        );
    }
}
