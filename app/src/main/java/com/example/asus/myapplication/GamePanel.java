package com.example.asus.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import static com.example.asus.myapplication.MainActivity.v;

public class GamePanel extends SurfaceView implements SurfaceHolder.Callback{
    private final Context context;
    private MainThread thread;
    private Rect r = new Rect();

    private Background bg;

    private Player player;
    private Point playerPoint;

    private EnemyManager enemyManager;
    private ArrayList<Enemy> enemies;
    private ArrayList<Bullet> bullets;
    private ArrayList<Bullet> enemyBullets;

    private boolean movingPlayer = false;
    private boolean gameOver = false;

    private long gameOverTime;
    private long bulletStartTime;
    private long bulletElapsed;

    private SQLiteDatabase db;
    DBManager dbmanager;
    private int score;

    public GamePanel(Context context){
        super(context);
        this.context = context;

        getHolder().addCallback(this);

        thread = new MainThread(getHolder(), this);

        dbmanager = new DBManager(context);
        db = dbmanager.getWritableDatabase();
        dbmanager.onCreate(db);

        setFocusable(true);
    }

    public void reset(){
        player.setHP(3);
        playerPoint = new Point(Constant.SCREEN_WIDTH/2, 3*Constant.SCREEN_HEIGHT/4);
        player.update(playerPoint);

        enemyManager = new EnemyManager(100, 100, Color.TRANSPARENT, 200, BitmapFactory.decodeResource(getResources(), R.drawable.enemy2));

        movingPlayer = false;

        bulletStartTime = System.nanoTime();
        bulletElapsed = 0;
        EnemyManager.score = 0;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height){

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder){
        EnemyManager.score = 0;

        bg = new Background(BitmapFactory.decodeResource(getResources(), R.drawable.back2));
        Constant.INIT_TIME = System.currentTimeMillis();
        player = new Player(new Rect(100,100,200,200), Color.TRANSPARENT, BitmapFactory.decodeResource(getResources(), R.drawable.main));
        playerPoint = new Point(Constant.SCREEN_WIDTH/2, 3*Constant.SCREEN_HEIGHT/4);
        player.update(playerPoint);

        bullets = new ArrayList<Bullet>();
        enemyBullets = new ArrayList<Bullet>();
        bulletStartTime = System.nanoTime();
        bulletElapsed = 0;

        enemyManager = new EnemyManager(100, 100, Color.TRANSPARENT, 200, BitmapFactory.decodeResource(getResources(), R.drawable.enemy2));

        thread.setRunning(true);
        thread.start();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder){
        boolean retry = true;

        while(retry){
            try{
                thread.setRunning(false);
                thread.join();
            } catch (Exception e) { e.printStackTrace(); }
            retry = false;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                if(!gameOver && player.getRectangle().contains((int)event.getX(), (int)event.getY()))
                    movingPlayer = true;
                if(gameOver && System.currentTimeMillis() - gameOverTime >= 2000){
                    reset();
                    gameOver = false;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if(!gameOver && movingPlayer)
                    playerPoint.set((int)event.getX(), (int)event.getY());
                break;
            case MotionEvent.ACTION_UP:
                movingPlayer = false;
                break;
        }

        return true;
    }


    public void update(){
        if(!gameOver) {
            bg.update();
            player.update(playerPoint);
            enemyManager.update();

            //generate bullet Player
            bulletElapsed += (System.nanoTime() - bulletStartTime) / 1000000;
            //System.out.println(bulletElapsed);
            if(bulletElapsed > 2000){
                bulletElapsed -= 2000;
                bullets.add(new Bullet(BitmapFactory.decodeResource(getResources(),R.drawable.bullet), player.getRectangle().centerX() - 30, player.getRectangle().centerY() - 50,50,50, 50));
            }
            bulletStartTime = System.nanoTime();
            for(Bullet bullet : bullets){
                bullet.update();

                if(enemyManager.bulletCollide(bullet)){
                    bullets.remove(bullet);
                    enemyManager.score++;
                }
                if(bullet.getX() < -50){
                    bullets.remove(bullet);
                }
            }

            //generate enemyBullet
            enemies = enemyManager.getEnemy();
            for(Enemy enemy : enemies){
                long enemyBulletElapsed = enemy.getBulletElapsed();
                enemyBulletElapsed += (System.nanoTime() - enemy.getBulletStartTime()) / 1000000;
                enemy.setBulletElapsed(enemyBulletElapsed);
                System.out.println(enemyBulletElapsed);
                if(enemyBulletElapsed > 5000 && enemy.getTop() > 200){
                    enemyBulletElapsed -= 5000;
                    enemyBullets.add(new Bullet(BitmapFactory.decodeResource(getResources(),R.drawable.enemy2_bullet), enemy.getRectangle().centerX() - 30, enemy.getRectangle().centerY() + 50,50,50, -35));
                    enemy.setBulletElapsed(enemyBulletElapsed);
                }
                enemy.setBulletStartTime(System.nanoTime());
            }

            //Kalo player nabrak enemyBullet
            for(Bullet enemyBullet : enemyBullets){
                enemyBullet.update();
                if(enemyBullet.getX() > Constant.SCREEN_HEIGHT){
                    enemyBullets.remove(enemyBullet);
                }
                if(enemyBullet.collide(player)){
                    enemyBullets.remove(enemyBullet);

                    int currHP = player.getHP();
                    currHP--;
                    player.setHP(currHP);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                    }else{
                        //deprecated in API 26
                        v.vibrate(500);
                    }

                    if(player.getHP() == 0) {
                        gameOver = true;
                        gameOverTime = System.currentTimeMillis();

                    }
                }
            }

            //Kalo Player nabrak Enemy
            if(enemyManager.playerCollide(player)){
                int currHP = player.getHP();
                currHP--;
                player.setHP(currHP);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                }else{
                    //deprecated in API 26
                    v.vibrate(500);
                }

                if(player.getHP() == 0) {
                    gameOver = true;
                    gameOverTime = System.currentTimeMillis();
                }

            }
        }
    }

    @Override
    public void draw(Canvas canvas){
        super.draw(canvas);

        canvas.drawColor(Color.WHITE);
        if(canvas!=null) {
            final int savedState = canvas.save();
            canvas.scale(4.0f, 4.0f);
            bg.draw(canvas);
            canvas.restoreToCount(savedState);
        }

        player.draw(canvas);
        for(Bullet bullet : bullets){
            bullet.draw(canvas);
        }
        for(Bullet enemyBullet : enemyBullets){
            enemyBullet.draw(canvas);
        }
        enemyManager.draw(canvas);

        Paint paint1 = new Paint();
        paint1.setTextSize(100);
        paint1.setColor(Color.MAGENTA);
        canvas.drawText("HP: " + player.getHP(), Constant.SCREEN_WIDTH - 300 + paint1.descent() - paint1.descent(), 100 + paint1.descent() - paint1.descent(), paint1);

        if(gameOver){
            //Paint paint = new Paint();
            //paint.setTextSize(100);
            //paint.setColor(Color.MAGENTA);
            //drawCenterText(canvas, paint, "Game Over");
            Intent gotoHs = new Intent(context, GameOver.class);
            gotoHs.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            score = enemyManager.getscore();
            gotoHs.putExtra("SCORE", score);
            context.startActivity(gotoHs);
        }
    }

    private void drawCenterText(Canvas canvas, Paint paint, String text){
        paint.setTextAlign(Paint.Align.LEFT);
        canvas.getClipBounds(r);
        int cHeight = r.height();
        int cWidth = r.width();
        paint.getTextBounds(text, 0, text.length(), r);
        float x = cWidth / 2f - r.width() / 2f - r.left;
        float y = cHeight / 2f - r.height() / 2f - r.bottom;
        canvas.drawText(text, x, y, paint);
    }

}
