package com.example.asus.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.ListView;

public class DBManager extends SQLiteOpenHelper {
    // Constants
    public static final String dbname = "highscore.db";
    public static final String tablename = "highscore";
    public static final String hs_name = "name";
    public static final String hs_score = "score";

    public DBManager(Context context) {
        super(context, dbname, null, 1);
    }

    public void onCreate(SQLiteDatabase db) {
        String CreateTable = "CREATE TABLE IF NOT EXISTS "+tablename+"("+
                hs_name+" TEXT, "+
                hs_score+" INTEGER "+")";
        db.execSQL(CreateTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void insertdata(SQLiteDatabase db, String name, int score){
        ContentValues myvalues = new ContentValues();
        myvalues.put(hs_name, name);
        myvalues.put(hs_score, score);
        db.insert(tablename, null, myvalues);
        db.close();
    }

    public Cursor getHighscores(){
        SQLiteDatabase db = this.getWritableDatabase();
        String query ="SELECT * FROM highscore ORDER BY score DESC LIMIT 3";
        Cursor data = db.rawQuery(query,null);
        return data;
    }

    public boolean isEmpty(){
        SQLiteDatabase db = getWritableDatabase();
        String count = "SELECT count(*) FROM highscore";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if(icount>0){ return false; }
        else{ return true; }
    }
}
